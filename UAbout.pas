unit UAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ShellAPI, jpeg, ComCtrls;

type
  TfrmAbout = class(TForm)
    grbAbout: TGroupBox;
    lblDescription: TLabel;
    btnClose: TButton;
    redtCopyright: TRichEdit;
    lblArusSoftLinkRu: TLabel;
    lblArusSoftLinkEnd: TLabel;
    procedure btnCloseClick(Sender: TObject);
    procedure lblArusSoftLinkRuClick(Sender: TObject);
    procedure lblArusSoftLinkEndClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAbout: TfrmAbout;

implementation

{$R *.dfm}

procedure TfrmAbout.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmAbout.lblArusSoftLinkRuClick(Sender: TObject);
begin
  ShellExecute(Handle,nil,'http://www.arussoft.ru',nil,nil,SW_SHOW);
end;

procedure TfrmAbout.lblArusSoftLinkEndClick(Sender: TObject);
begin
  ShellExecute(Handle,nil,'http://www.arussoft.com',nil,nil,SW_SHOW);
end;

end.
