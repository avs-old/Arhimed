unit UTypes;

interface

uses Graphics, Controls;

type
  TOptions = record
    EnabledTrayIcon: Boolean;

    EnabledAlphaBlend: Boolean;
    AlphaBlend: Integer;
//    Align: TAlign;

    StayOnTop: Boolean;

//    Height, Width,
    Left, Top: Integer;

    ButtonColor: TColor;

    Language: Boolean; // default language English - true

    StateWork: byte;  // calculator mode 1 - full, 2 - simple, 3 - mini
  end;

implementation

end.
