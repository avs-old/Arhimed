object frmOptions: TfrmOptions
  Left = 530
  Top = 150
  Width = 229
  Height = 146
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblNumberColor: TLabel
    Left = 8
    Top = 40
    Width = 54
    Height = 13
    Caption = #1062#1074#1077#1090' '#1094#1080#1092#1088
  end
  object btnSave: TButton
    Left = 8
    Top = 88
    Width = 65
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 0
    OnClick = btnSaveClick
  end
  object btnCancel: TButton
    Left = 160
    Top = 88
    Width = 57
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    OnClick = btnCancelClick
  end
  object btnReset: TButton
    Left = 80
    Top = 88
    Width = 75
    Height = 25
    Caption = #1057#1090#1072#1085#1076#1072#1088#1090#1085#1099#1077
    TabOrder = 2
    OnClick = btnResetClick
  end
  object chbTrayIcon: TCheckBox
    Left = 8
    Top = 64
    Width = 97
    Height = 17
    Caption = #1048#1082#1086#1085#1082#1072' '#1074' '#1090#1088#1077#1077
    TabOrder = 3
  end
  object chbAlphaBlend: TCheckBox
    Left = 8
    Top = 8
    Width = 145
    Height = 17
    Caption = #1042#1082#1083#1102#1095#1080#1090#1100' '#1087#1088#1086#1079#1088#1072#1095#1085#1086#1089#1090#1100
    TabOrder = 4
    OnClick = chbAlphaBlendClick
  end
  object chbStayOnTop: TCheckBox
    Left = 128
    Top = 64
    Width = 89
    Height = 17
    Caption = #1055#1086#1074#1077#1088#1093' '#1086#1082#1086#1085
    TabOrder = 5
  end
  object spdAlphaBlend: TSpinEdit
    Left = 160
    Top = 3
    Width = 57
    Height = 22
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxValue = 255
    MinValue = 0
    ParentFont = False
    TabOrder = 6
    Value = 0
  end
  object clbButtonColor: TColorBox
    Left = 104
    Top = 32
    Width = 113
    Height = 22
    ItemHeight = 16
    TabOrder = 7
  end
end
