unit UGlobal;
// ��������� ���� ��� ����� �������

interface

uses Graphics, SysUtils, Math, IniFiles, UTypes;

procedure LoadOptions;
procedure SaveOptions;

function Faktorial(Num:integer):Extended;
Function CalcRes(Res:string;var Res2:Extended;var Error:integer;var NumSim:integer):boolean;

var
  WorkDirectory: String;
  OptionFile: TIniFile;

  Options: TOptions;

  Memory, Memory2: Extended;  // ������
  Result: Extended;  // ��������� ����������
  ErrCode,ErrSim: Integer;  //  kod error, number simbol error
  StateGrads: (Rad,Grad,Deg); // ��� �������� ����

  // For SimpleCalc
  Point: Boolean; // ���� �� �������
  PressCalc: Boolean; // ������ �� ������ ���������
  PressOper: Boolean;
  Operation: Byte; // �������������� ��������

  FirstOper: Double;
  SecondOper: Double;

implementation

uses UMain;

procedure LoadOptions;
begin
  //��������� ���������
  OptionFile:=TIniFile.Create(PChar(WorkDirectory+'arhimed.ini'));

  try
    with Options do
    begin
      EnabledTrayIcon:=OptionFile.ReadBool('Options','TrayIcon',true);

      EnabledAlphaBlend:=OptionFile.ReadBool('Options','EnabledAlphaBlend',false);
      AlphaBlend:=OptionFile.ReadInteger('Options','AlphaBlend',0);

      StayOnTop:=OptionFile.ReadBool('Options','StayOnTop',false);

      ButtonColor:=OptionFile.ReadInteger('Options','ButtonColor',16711680);

      Left:=OptionFile.ReadInteger('Options','Left',0);
      Top:=OptionFile.ReadInteger('Options','Top',0);

      Language:=OptionFile.ReadBool('Options','Language',true);

      StateWork:=OptionFile.ReadInteger('Options','StateWork',2);
    end;
  finally
    OptionFile.Free;
  end;

end;

procedure SaveOptions;
begin
  //��������� ���������
  OptionFile:=TIniFile.Create(PChar(WorkDirectory+'arhimed.ini'));

  try
    with Options do
    begin
      OptionFile.WriteBool('Options','TrayIcon',EnabledTrayIcon);

      OptionFile.WriteBool('Options','EnabledAlphaBlend',EnabledAlphaBlend);
      OptionFile.WriteInteger('Options','AlphaBlend',AlphaBlend);

      OptionFile.WriteBool('Options','StayOnTop',StayOnTop);

      OptionFile.WriteInteger('Options','ButtonColor',ButtonColor);

      OptionFile.WriteInteger('Options','Left',frmMain.Left);
      OptionFile.WriteInteger('Options','Top',frmMain.Top);

      OptionFile.WriteBool('Options','Language',Language);

      OptionFile.WriteInteger('Options','StateWork',StateWork);
    end;
  finally
    OptionFile.Free;
  end;
end;

//-----------------------------------------------------------------
function Faktorial(Num:integer):Extended;
var
  j:integer;
  f:Extended;
begin
  f:=1;
  for j:=2 to Num do f:=f*j;
  Faktorial:=f;
end;
//-----------------------------------------------------------------
// ������� ��������� ������� Res � ��������� � �� ������������
// Res2 - ��������� ����������; Error - ��� ������ (1 - ������ ������� � �����;2 - ������������ ������(����);3 - ������� �� ����);
// NumSim - ������ ������ (��� ����� ��������)
Function CalcRes(Res:string;var Res2:Extended;var Error:integer;var NumSim:integer):boolean;
var i:integer;

function E:Extended; forward;

function F:Extended;
var
  tmp:string;
  x:Extended;
begin
  case Res[i] of
    '(':begin
          inc(i);
          x:=E;
          if Res[i]=')' then
          begin
            inc(i);
            if Res[i]='!' then
            begin
              inc(i);
              x:=Faktorial(Trunc(x));
            end;
            F:=x;
          end
          else
          begin

          end;
        end;
    '0'..'9',',':begin
          while Res[i] in ['0','1','2','3','4','5','6','7','8','9',','] do
          begin
            tmp:=tmp+Res[i];inc(i);
          end;
          x:=StrToFloat(tmp);
          if Res[i]='!' then
          begin
            x:=Faktorial(Trunc(x));inc(i);
          end;
          F:=x;
        end;
    'c':begin
          if (Res[i+1]='o') and (Res[i+2]='s') then
          begin
            // ���������� ��������
            i:=i+4;
            x:=E;inc(i);
            if StateGrads=Deg then F:=cos(DegToRad(x))
            else if StateGrads=Grad then F:=cos(GradToRad(x))
              else F:=cos(x);
          end
          else
            if (Res[i+1]='t') and (Res[i+2]='a') and (Res[i+3]='n') then
            begin
              // ���������� ����������
              i:=i+5;
              x:=E;inc(i);
              if StateGrads=Deg then F:=cotan(DegToRad(x))
              else if StateGrads=Grad then F:=cotan(GradToRad(x))
                else F:=cotan(x);
            end;
        end;
    's':begin
          if (Res[i+1]='i') and (Res[i+2]='n') then
          begin
            // ���������� ������
            i:=i+4;
            x:=E;inc(i);
            if StateGrads=Deg then F:=sin(DegToRad(x))
            else if StateGrads=Grad then F:=sin(GradToRad(x))
              else F:=sin(x);
          end;
        end;
    't':begin
          if (Res[i+1]='a') and (Res[i+2]='n') then
          begin
            // ���������� ��������
            i:=i+4;
            x:=E;inc(i);
            if StateGrads=Deg then F:=tan(DegToRad(x))
            else if StateGrads=Grad then F:=tan(GradToRad(x))
              else F:=tan(x);
          end;
        end;
   'l':begin
          if Res[i+1]='n' then
          begin
            // ���������� ������������ ���������
            i:=i+3;
            x:=E;inc(i);
            F:=ln(x); // � ��������
          end
          else
            if (Res[i+1]='o') and (Res[i+2]='g') then
            begin
              // ���������� ����������� ���������
              i:=i+4;
              x:=E;inc(i);
              F:=log10(x); // � ��������
            end;
        end;
   else
     Error:=2;
     NumSim:=i;
   end;
end;

function K:Extended;
var x:Extended;flag:boolean;
begin
  if Error=0 then
  begin
    x:=F;
    flag:=true;
    while flag do
      if Res[i]='^' then
      begin
        inc(i);
        x:=Power(x,K);
      end
      else begin
        K:=x;
        flag:=false;
      end;
  end
  else K:=0;
end;

function T:Extended;
var
  x,x2:Extended;
  flag:boolean;
  l:integer;
begin
  if Error=0 then
  begin
    x:=K;
    flag:=true;
    while flag do
    case Res[i] of
       '*':begin
             inc(i);
             x:=x*K;
           end;
       '/':begin
             l:=i;
             inc(i);
             x2:=K;
             if x2<>0 then x:=x/x2
             else
             begin
               Error:=3;
               NumSim:=l+1;
             end;
           end;
       else begin
           flag:=false;
           T:=x;
       end;
    end;
  end
  else T:=0;
end;

function E:Extended;
var x:Extended;flag:boolean;
begin
  if Error=0 then
  begin
    x:=T;
    flag:=true;
    while flag do
      case Res[i] of
         '+':begin
               inc(i);
               x:=x+T;
             end;
         '-':begin
               inc(i);
               x:=x-T;
             end;
         else begin
           flag:=false;
           E:=x;
         end;
      end;
  end
  else E:=0;
end;

begin
  // ������� �������� ������� ��������� � ������ �������
  i:=1;
  Res:=LowerCase(Res);
  while i<(length(Res)+1) do
    if Res[i]=' ' then Delete(Res,i,1)
    else inc(i);

  Error:=0;
  i:=1;

  Res2:=E;

//  ���� � ����� ������ ��������� ����� ������������ ��� ���������� ������
//  ����� ���� �� ����� ��������
{  if i<(length(Res)+1) then
  begin
    Error:=1;
    NumSim:=i;
  end;}

  if Error<>0 then CalcRes:=false
  else CalcRes:=true;
end;
//-----------------------------------------------------------------

end.
