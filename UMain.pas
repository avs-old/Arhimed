//Copyright (c) 2013, ArusSoft (Alexander Selivanov) - http://www.arussoft.com
//
// Arus Arhimed
// Copyright (c) 2011-2013, ArusSoft (Alexander Selivanov) - http://www.arussoft.com
// All rights reserved.
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//       - Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//       - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//       - Neither the name of the ArusSoft nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Arus Arhimed
// Copyright (c) 2011-2013, ArusSoft (��������� ���������)  - http://www.arussoft.ru
// ��� ����� ��������.
// ����������� ��������� ��������������� � ������������� ��� � ���� ��������� ����, ��� � � �������� �����, � ����������� ��� ���, ��� ���������� ��������� �������:
//       - ��� ��������� ��������������� ��������� ���� ������ ���������� ��������� ���� ����������� �� ��������� �����, ���� ������ ������� � ����������� ����� �� ��������.
//       - ��� ��������� ��������������� ��������� ���� ������ ����������� ��������� ���� ���������� �� ��������� �����, ���� ������ ������� � ����������� ����� �� �������� � ������������ �/��� � ������ ����������, ������������ ��� ���������������.
//       - �� �������� ArusSoft, �� ����� �� ����������� �� ����� ���� ������������ � �������� ��������� ��� ����������� ���������, ���������� �� ���� �� ��� ���������������� ����������� ����������.
// ��� ��������� ������������� ����������� ��������� ���� �/��� ������� ��������� <��� ��� ����> ��� ������-���� ���� ��������, ���������� ���� ��� ���������������, �������, �� �� ������������� ���, ��������������� �������� ������������ �������� � ����������� ��� ���������� ����. �� � ���� ������, ���� �� ��������� ��������������� �������, ��� �� ����������� � ������ �����, �� ���� �������� ��������� ���� � �� ���� ������ ����, ������� ����� �������� �/��� �������� �������������� ���������, ��� ���� ������� ����, �� ��Ѩ� ���������������, ������� ����� �����, ���������, ����������� ��� ������������� ������, ���������� ������������� ��� ������������� ������������� ��������� (�������, �� �� ������������� ������� ������, ��� �������, �������� �������������, ��� �������� ������������ ��-�� ��� ��� ������� ���, ��� ������� ��������� �������� ��������� � ������� �����������), ���� ���� ����� �������� ��� ������ ���� ���� �������� � ����������� ����� �������.

unit UMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ImgList, Menus, ActnList,  ExtCtrls,
  ToolWin, XPMan, Buttons, Math, StrUtils, Clipbrd, ShellAPI;

const
  WM_MYTRAYNOTIFY=WM_USER+1;

type
  TfrmMain = class(TForm)
    redtFormula: TRichEdit;
    imglstMain: TImageList;
    mnuResult: TPopupMenu;
    XPMan: TXPManifest;
    grbGrads: TGroupBox;
    rbtnGrads: TRadioButton;
    rbtnRadians: TRadioButton;
    rbtnDegrees: TRadioButton;
    grbResult: TGroupBox;
    lblResult: TLabel;
    btnNum1: TBitBtn;
    btnNum2: TBitBtn;
    btnNum3: TBitBtn;
    btnNum4: TBitBtn;
    btnNum5: TBitBtn;
    btnNum6: TBitBtn;
    btnNum7: TBitBtn;
    btnNum8: TBitBtn;
    btnNum9: TBitBtn;
    btnPoint: TBitBtn;
    btnNum0: TBitBtn;
    btnSign: TBitBtn;
    btnDivide: TBitBtn;
    btnMultiplie: TBitBtn;
    btnSubtrack: TBitBtn;
    btnAdd: TBitBtn;
    btnMemC: TBitBtn;
    btnMemR: TBitBtn;
    btnMemSubtrack: TBitBtn;
    btnMemAdd: TBitBtn;
    grbMemory: TGroupBox;
    lblMemory: TLabel;
    btnSin: TBitBtn;
    btnCos: TBitBtn;
    btnTan: TBitBtn;
    btnCtan: TBitBtn;
    btnBracketClose: TBitBtn;
    btnBracketOpen: TBitBtn;
    btnExp: TBitBtn;
    btnLn: TBitBtn;
    btnLog: TBitBtn;
    btnFact: TBitBtn;
    btnOneDivide: TBitBtn;
    btnPi: TBitBtn;
    btnDegree2: TBitBtn;
    btnDegree3: TBitBtn;
    btnDegree: TBitBtn;
    ToolBar1: TToolBar;
    tbtnFullCalc: TToolButton;
    btnClear: TBitBtn;
    btnCalc: TBitBtn;
    tbtnSimpleCalc: TToolButton;
    tbtnMiniCalc: TToolButton;
    btnMemC2: TBitBtn;
    btnMemR2: TBitBtn;
    btnMemSubtrack2: TBitBtn;
    btnMemAdd2: TBitBtn;
    grbMemory2: TGroupBox;
    lblMemory2: TLabel;
    Label2: TLabel;
    mnuCopy: TMenuItem;
    N1: TMenuItem;
    mnuLessFont: TMenuItem;
    mnuBiggerFont: TMenuItem;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    tbtnAbout: TToolButton;
    tbtnOptions: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    tbtnEnglish: TToolButton;
    tbtnRussian: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    procedure btnNum1Click(Sender: TObject);
    procedure btnNum2Click(Sender: TObject);
    procedure btnNum3Click(Sender: TObject);
    procedure btnNum4Click(Sender: TObject);
    procedure btnNum5Click(Sender: TObject);
    procedure btnNum6Click(Sender: TObject);
    procedure btnNum7Click(Sender: TObject);
    procedure btnNum8Click(Sender: TObject);
    procedure btnNum9Click(Sender: TObject);
    procedure btnNum0Click(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnCalcClick(Sender: TObject);
    procedure btnPointClick(Sender: TObject);
    procedure btnMemRClick(Sender: TObject);
    procedure btnMemCClick(Sender: TObject);
    procedure btnMemAddClick(Sender: TObject);
    procedure btnMemSubtrackClick(Sender: TObject);
    procedure btnMemC2Click(Sender: TObject);
    procedure btnMemR2Click(Sender: TObject);
    procedure btnMemSubtrack2Click(Sender: TObject);
    procedure btnMemAdd2Click(Sender: TObject);
    procedure redtFormulaKeyPress(Sender: TObject; var Key: Char);
    procedure tbtnMiniCalcClick(Sender: TObject);
    procedure tbtnFullCalcClick(Sender: TObject);
    procedure tbtnSimpleCalcClick(Sender: TObject);
    procedure btnDivideClick(Sender: TObject);
    procedure btnMultiplieClick(Sender: TObject);
    procedure btnSubtrackClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure redtFormulaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnBracketOpenClick(Sender: TObject);
    procedure btnBracketCloseClick(Sender: TObject);
    procedure btnFactClick(Sender: TObject);
    procedure btnDegreeClick(Sender: TObject);
    procedure btnSinClick(Sender: TObject);
    procedure btnCosClick(Sender: TObject);
    procedure btnTanClick(Sender: TObject);
    procedure btnCtanClick(Sender: TObject);
    procedure btnLnClick(Sender: TObject);
    procedure btnLogClick(Sender: TObject);
    procedure btnDegree3Click(Sender: TObject);
    procedure btnDegree2Click(Sender: TObject);
    procedure btnPiClick(Sender: TObject);
    procedure btnExpClick(Sender: TObject);
    procedure rbtnRadiansClick(Sender: TObject);
    procedure rbtnGradsClick(Sender: TObject);
    procedure rbtnDegreesClick(Sender: TObject);
    procedure mnuCopyClick(Sender: TObject);
    procedure mnuLessFontClick(Sender: TObject);
    procedure mnuBiggerFontClick(Sender: TObject);
    procedure redtFormulaChange(Sender: TObject);
    procedure btnOneDivideClick(Sender: TObject);
    procedure tbtnAboutClick(Sender: TObject);
    procedure tbtnOptionsClick(Sender: TObject);
    //------------------------------------------------------
    procedure MarketText;
    function CheckSimbol(S:char):boolean;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

    procedure MsgIcon(var Msg:TMessage);message WM_MYTRAYNOTIFY;

    procedure SetOptions;
    procedure tbtnEnglishClick(Sender: TObject);
    procedure tbtnRussianClick(Sender: TObject);
    //------------------------------------------------------
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses UAbout, UTypes, UOptions, UGlobal;

{$R *.dfm}

//-----------
procedure TfrmMain.MsgIcon(var Msg:TMessage);
var Pt:TPoint;
begin
  case Msg.lParam of
    WM_LBUTTONDOWN:begin
      SetForegroundWindow(Handle);
      ShowWindow(Handle,SW_RESTORE);
    end;
    WM_RBUTTONDOWN:begin
      GetCursorPos(Pt);
//      pmnTrayIcon.Popup(Pt.X,Pt.Y);
    end;
  end;
end;
//--------------------------------------------

//---------------------------
procedure TfrmMain.SetOptions;
var Tray:TNotifyIconData;
    i:integer;
begin
  with Options do
  begin
    btnNum1.Font.Color:=ButtonColor;
    btnNum2.Font.Color:=ButtonColor;
    btnNum3.Font.Color:=ButtonColor;
    btnNum4.Font.Color:=ButtonColor;
    btnNum5.Font.Color:=ButtonColor;
    btnNum6.Font.Color:=ButtonColor;
    btnNum7.Font.Color:=ButtonColor;
    btnNum8.Font.Color:=ButtonColor;
    btnNum9.Font.Color:=ButtonColor;
    btnNum0.Font.Color:=ButtonColor;
    btnPoint.Font.Color:=ButtonColor;
    btnSign.Font.Color:=ButtonColor;

    btnClear.Font.Color:=ButtonColor;
    btnCalc.Font.Color:=ButtonColor;

    lblResult.Font.Color:=ButtonColor;

    frmMain.AlphaBlend:=EnabledAlphaBlend;
    frmMain.AlphaBlendValue:=AlphaBlend;
//    frmMain.Align:=Align;

    if StayOnTop then
    begin
      FormStyle:=fsStayOnTop;
      frmOptions.FormStyle:=fsStayOnTop;
      frmAbout.FormStyle:=fsStayOnTop;
    end
    else
    begin
      FormStyle:=fsNormal;
      frmOptions.FormStyle:=fsNormal;
      frmAbout.FormStyle:=fsNormal;
    end;


//    frmMain.Height:=Height;
//    frmMain.Width:=Width;
    frmMain.Left:=Left;
    frmMain.Top:=Top;

    if Language then
    begin
      tbtnEnglishClick(Self);
    end
    else
    begin
      tbtnRussianClick(Self);
    end;


    if EnabledTrayIcon then
    begin
      with Tray do
      begin
        cbSize:=SizeOf(TNotifyIconData);
        Wnd:=Handle;
        uId:=1;
        uFlags:=NIF_ICON or NIF_MESSAGE or NIF_TIP;
        uCallBackMessage:=WM_MYTRAYNOTIFY;
        hIcon:=Application.Icon.Handle;
        if Options.Language then
        begin
          StrPCopy(szTip,'Arhimed');
        end
        else
        begin
          StrPCopy(szTip,'�������');
        end
      end;
      Shell_NotifyIcon(NIM_ADD,@Tray);
      ShowWindow(Application.Handle,SW_HIDE);
    end;

    if not EnabledTrayIcon then
    begin
      with Tray do
      begin
        cbSize:=SizeOf(TNotifyIconData);
        Wnd:=Handle;
        uId:=1;
        uFlags:=NIF_ICON or NIF_MESSAGE or NIF_TIP;
        uCallBackMessage:=WM_MYTRAYNOTIFY;
        hIcon:=Application.Icon.Handle;
        if Options.Language then
        begin
          StrPCopy(szTip,'Arhimed');
        end
        else
        begin
          StrPCopy(szTip,'�������');
        end
      end;
      Shell_NotifyIcon(NIM_DELETE,@Tray);
      ShowWindow(Application.Handle,SW_SHOWNORMAL);
    end;

    MarketText;
  end;
end;
//---------------------------


procedure TfrmMain.MarketText;  // text colorize
var
  posCur,i:integer;
  Expression:string;
begin
    redtFormula.Lines.BeginUpdate;
    posCur:=redtFormula.SelStart;
    Expression:=redtFormula.Text;
    for i:=1 to Length(Expression) do
      case Expression[i] of
        '(',')':begin
                redtFormula.SelStart:=i-1;
                redtFormula.SelLength:=1;
                redtFormula.SelAttributes.Color:=clGreen;
              end;
        '+','-','*','/':begin
                redtFormula.SelStart:=i-1;
                redtFormula.SelLength:=1;
                redtFormula.SelAttributes.Color:=clRed;
              end;
        '^','!':begin
                redtFormula.SelStart:=i-1;
                redtFormula.SelLength:=1;
                redtFormula.SelAttributes.Color:=clNavy;
              end;
        '0'..'9',',':begin
                redtFormula.SelStart:=i-1;
                redtFormula.SelLength:=1;
                redtFormula.SelAttributes.Color:=Options.ButtonColor;
              end;
        'c','o','s','i','n','t','a','l','g':begin
                redtFormula.SelStart:=i-1;
                redtFormula.SelLength:=1;
                redtFormula.SelAttributes.Color:=clBlack;
              end;
      end;
    // find function
    if Length(Expression)>=3 then
    begin
      // find cos
      i:=0;
      repeat
        i:=PosEx('cos',Expression,i+1);
        if i<>0 then
        begin
          redtFormula.SelStart:=i-1;
          redtFormula.SelLength:=3;
          redtFormula.SelAttributes.Color:=clFuchsia;
        end;
      until i=0;
      // find sin
      i:=0;
      repeat
        i:=PosEx('sin',Expression,i+1);
        if i<>0 then
        begin
          redtFormula.SelStart:=i-1;
          redtFormula.SelLength:=3;
          redtFormula.SelAttributes.Color:=clFuchsia;
        end;
      until i=0;
      // find tan
      i:=0;
      repeat
        i:=PosEx('tan',Expression,i+1);
        if i<>0 then
        begin
          redtFormula.SelStart:=i-1;
          redtFormula.SelLength:=3;
          redtFormula.SelAttributes.Color:=clFuchsia;
        end;
      until i=0;
      // find log
      i:=0;
      repeat
        i:=PosEx('log',Expression,i+1);
        if i<>0 then
        begin
          redtFormula.SelStart:=i-1;
          redtFormula.SelLength:=3;
          redtFormula.SelAttributes.Color:=clFuchsia;
        end;
      until i=0;
      // find ln
      i:=0;
      repeat
        i:=PosEx('ln',Expression,i+1);
        if i<>0 then
        begin
          redtFormula.SelStart:=i-1;
          redtFormula.SelLength:=2;
          redtFormula.SelAttributes.Color:=clFuchsia;
        end;
      until i=0;
      // find ctan
      i:=0;
      repeat
        i:=PosEx('ctan',Expression,i+1);
        if i<>0 then
        begin
          redtFormula.SelStart:=i-1;
          redtFormula.SelLength:=4;
          redtFormula.SelAttributes.Color:=clFuchsia;
        end;
      until i=0;
    end;
    redtFormula.SelStart:=posCur;
    redtFormula.Lines.EndUpdate;
end;
//-----------------------------------------------------------------
function TfrmMain.CheckSimbol(S:char):boolean; // check symbol in current expression place S
var
  Expression:string;
begin
  CheckSimbol:=true;
  Expression:=redtFormula.Text;
  case S of
    '1','2','3','4','5','6','7','8','9','0':begin
      end;
    ',':begin
        if redtFormula.SelStart=0 then CheckSimbol:=false
          else
            if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['0','1','2','3','4','5','6','7','8','9']))then CheckSimbol:=false
            else
              if (redtFormula.SelStart<Length(Expression))and(Expression[redtFormula.SelStart+1]in['(',','])then CheckSimbol:=false;
        end;
    '!':begin
        if redtFormula.SelStart=0 then CheckSimbol:=false
        else
          if (redtFormula.SelStart>0)and(Expression[redtFormula.SelStart]in['+','-','*','/','^','!',','])then CheckSimbol:=false
          else
            if (redtFormula.SelStart<Length(Expression))and(Expression[redtFormula.SelStart+1]in['(',','])then CheckSimbol:=false;
      end;
    '^','/','*','-','+':begin
        if redtFormula.SelStart=0 then CheckSimbol:=false
        else
          if Expression[redtFormula.SelStart]in['/','*','-','+','^',',']then CheckSimbol:=false
          else
            if (redtFormula.SelStart<Length(Expression))and(Expression[redtFormula.SelStart+1]in['/','*','-','+','^',',','c','s','t','l'])then CheckSimbol:=false;
      end;
    'c':begin
        // cos, ctan
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['/','*','-','+','(']))then CheckSimbol:=false
      end;
    'o':begin
        // cos, log
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['l','c']))then CheckSimbol:=false
      end;
    's':begin
        // sin, cos
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['/','*','-','+','(','o']))then CheckSimbol:=false
      end;
    'i':begin
        // sin
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['s']))then CheckSimbol:=false
      end;
    'n':begin
        // sin, ln, tan, ctan
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['i','l','a']))then CheckSimbol:=false
      end;
    't':begin
        //tan, ctan
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['/','*','-','+','(','c']))then CheckSimbol:=false
      end;
    'a':begin
        //tan, ctan
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['t']))then CheckSimbol:=false
      end;
    'l':begin
        // ln, log
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['/','*','-','+','(']))then CheckSimbol:=false
      end;
    'g':begin
        // log
        if (redtFormula.SelStart>0)and(not(Expression[redtFormula.SelStart]in['o']))then CheckSimbol:=false
      end;
    '(':begin
        if (redtFormula.SelStart>0)and(Expression[redtFormula.SelStart]in[','])then CheckSimbol:=false
        else
          if (redtFormula.SelStart<Length(Expression))and(Expression[redtFormula.SelStart+1]in['+','-','*','/','^','!',','])then CheckSimbol:=false;
      end;
    ')':begin
        if redtFormula.SelStart=0 then CheckSimbol:=false
        else
          if (redtFormula.SelStart>0)and(Expression[redtFormula.SelStart]in['+','-','*','/','^','!',','])then CheckSimbol:=false
          else
            if (redtFormula.SelStart<Length(Expression))and(Expression[redtFormula.SelStart+1]in[','])then CheckSimbol:=false;
      end;
  else CheckSimbol:=false;
  end;
end;
//-----------------------------------------------------------------

procedure TfrmMain.btnNum1Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('1') then
    begin
      redtFormula.SelText:='1';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='1';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='1';
end;

procedure TfrmMain.btnNum2Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('2') then
    begin
      redtFormula.SelText:='2';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='2';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='2';
end;

procedure TfrmMain.btnNum3Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('3') then
    begin
      redtFormula.SelText:='3';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='3';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='3';
end;

procedure TfrmMain.btnNum4Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('4') then
    begin
      redtFormula.SelText:='4';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='4';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='4';
end;

procedure TfrmMain.btnNum5Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('5') then
    begin
      redtFormula.SelText:='5';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='5';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='5';
end;

procedure TfrmMain.btnNum6Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('6') then
    begin
      redtFormula.SelText:='6';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='6';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='6';
end;

procedure TfrmMain.btnNum7Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('7') then
    begin
      redtFormula.SelText:='7';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='7';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='7';
end;

procedure TfrmMain.btnNum8Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('8') then
    begin
      redtFormula.SelText:='8';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='8';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='8';
end;

procedure TfrmMain.btnNum9Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('9') then
    begin
      redtFormula.SelText:='9';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='9';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='9';
end;

procedure TfrmMain.btnNum0Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('0') then
    begin
      redtFormula.SelText:='0';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0') or PressCalc or PressOper then
    begin
      redtFormula.Text:='0';
      redtFormula.SelStart:=1;
      PressCalc:=false;
      PressOper:=false;
    end
    else redtFormula.SelText:='0';
end;

procedure TfrmMain.btnClearClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
    redtFormula.Clear
  else
  begin
    redtFormula.Text:='0';
    Operation:=0;
    FirstOper:=0;
    Point:=false;
    PressCalc:=false;
  end;
end;

procedure TfrmMain.btnCalcClick(Sender: TObject);
begin
  try
    if Options.StateWork<>2 then
    begin
      // TODO: check brackets number and position

      if Length(redtFormula.Text)>0 then
        if CalcRes(redtFormula.Text,Result,ErrCode,ErrSim) then
        begin
          lblResult.Caption:=FloatToStr(Result);  //FloatToStrF(Result,ffNumber,18,10)
          lblResult.Hint:=FloatToStr(Result);
        end
        else
        begin
          // halt error ErrCode - error code, ErrSim - expression symbol where parser halt error
          lblResult.Font.Size:=8;
          if ErrCode=3 then
          begin
            if Options.Language then
              lblResult.Caption:='Division by zero(symbol �'+IntToStr(ErrSim)+')'
            else
              lblResult.Caption:='������� �� ����(������ �'+IntToStr(ErrSim)+')';
          end
          else
          begin
            if Options.Language then
              lblResult.Caption:='Error(symbol �'+IntToStr(ErrSim)+')'
            else
              lblResult.Caption:='��������� ������(������ �'+IntToStr(ErrSim)+')';
          end;
        end;
    end
    else
    begin
      if not PressCalc then SecondOper:=StrToFloat(redtFormula.Text);
      if (SecondOper=0)and(Operation=4) then
      begin
        if Options.Language then
          redtFormula.Text:='Division by zero'
        else
          redtFormula.Text:='������� �� ����';
      end
      else
      begin
        if Operation<>0 then
          case Operation of
            1:redtFormula.Text:=FloatToStr(FirstOper+SecondOper);
            2:redtFormula.Text:=FloatToStr(FirstOper-SecondOper);
            3:redtFormula.Text:=FloatToStr(FirstOper*SecondOper);
            4:redtFormula.Text:=FloatToStr(FirstOper/SecondOper);
            5:redtFormula.Text:=FloatToStr(Power(FirstOper,SecondOper));
          end;
        FirstOper:=StrToFloat(redtFormula.Text);
        Point:=false;
      end;
      PressCalc:=true;
    end;
  except
    on EOverflow do
    begin
      lblResult.Font.Size:=8;
      if Options.Language then
        lblResult.Caption:='Everflow'
      else
        lblResult.Caption:='������� ������� �����';
    end
    else
    begin
      lblResult.Font.Size:=8;
      if Options.Language then
        lblResult.Caption:='Unknown error'
      else
        lblResult.Caption:='��������� ����������� ������';
    end;
  end;
end;

procedure TfrmMain.btnPointClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol(',') then
    begin
      redtFormula.SelText:=',';
      MarketText;
    end;
  end
  else
    if (redtFormula.Text='0')or PressCalc then
    begin
      redtFormula.Text:='0,';
      redtFormula.SelStart:=2;
      PressCalc:=false;
      Point:=true;
    end
    else
    if not Point then
    begin
      redtFormula.SelText:=',';
      Point:=true;
    end;
end;

procedure TfrmMain.btnMemRClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    redtFormula.SelText:=FloatToStr(Memory);
    MarketText;
  end
  else
    redtFormula.Text:=FloatToStr(Memory);
end;

procedure TfrmMain.btnMemCClick(Sender: TObject);
begin
  Memory:=0;
  lblMemory.Caption:='0';
  lblMemory.Hint:='0';
end;

procedure TfrmMain.btnMemAddClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
    Memory:=Memory+Result
  else
    Memory:=Memory+StrToFloat(redtFormula.Text);

  lblMemory.Caption:=FloatToStrF(Memory,ffGeneral,5,2);
  lblMemory.Hint:=FloatToStr(Memory);
end;

procedure TfrmMain.btnMemSubtrackClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
    Memory:=Memory-Result
  else
    Memory:=Memory-StrToFloat(redtFormula.Text);

  lblMemory.Caption:=FloatToStrF(Memory,ffGeneral,5,2);
  lblMemory.Hint:=FloatToStr(Memory);
end;

procedure TfrmMain.btnMemC2Click(Sender: TObject);
begin
  Memory2:=0;
  lblMemory2.Caption:='0';
  lblMemory2.Hint:='0';
end;

procedure TfrmMain.btnMemR2Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    redtFormula.SelText:=FloatToStr(Memory2);
    MarketText;
  end
  else
    redtFormula.Text:=FloatToStr(Memory2);
end;

procedure TfrmMain.btnMemSubtrack2Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
    Memory2:=Memory2-Result
  else
    Memory2:=Memory2-StrToFloat(redtFormula.Text);

  lblMemory2.Caption:=FloatToStrF(Memory2,ffGeneral,8,2);
  lblMemory2.Hint:=FloatToStr(Memory2);
end;

procedure TfrmMain.btnMemAdd2Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
    Memory2:=Memory2+Result
  else
    Memory2:=Memory2+StrToFloat(redtFormula.Text);

  lblMemory2.Caption:=FloatToStrF(Memory2,ffGeneral,8,2);
  lblMemory2.Hint:=FloatToStr(Memory2);
end;

procedure TfrmMain.redtFormulaKeyPress(Sender: TObject; var Key: Char);
var
  tmp:string;
begin
  if Options.StateWork<>2 then
    if Key='.' then Key:=',';
    if Ord(Key)=13 then
    begin
      Key:=#0;
      btnCalcClick(Self);
    end
    else
      if Ord(Key)<>8 then
      begin
        tmp:=LowerCase(Key);
        if CheckSimbol(tmp[1]) then Key:=tmp[1]
        else Key:=#0;
      end;
  if Options.StateWork=2then
  begin
    if PressCalc and (Key in ['1','2','3','4','5','6','7','8','9','0',',']) then redtFormula.Text:='';
    if (Key=',') and Point then Key:=#0;
    if (not(Key in ['1','2','3','4','5','6','7','8','9','0',','])) then Key:=#0;
    if (Key in ['1','2','3','4','5','6','7','8','9','0'])and(redtFormula.Text='0') then
    begin
      redtFormula.Text:=Key;
      redtFormula.SelStart:=1;
      Key:=#0;
    end;
    if (Key=',')and(redtFormula.Text='0') then
    begin
      redtFormula.Text:='0,';
      redtFormula.SelStart:=2;
      Key:=#0;
      Point:=true;
    end;
    if (Key=',')and(not Point) then Point:=true;
  end;

end;

procedure TfrmMain.tbtnMiniCalcClick(Sender: TObject);
begin
  Height:=144;

  if Options.StateWork=2 then
  begin
    btnBracketOpen.Enabled:=true;
    btnBracketClose.Enabled:=true;

    redtFormula.Clear;
    redtFormula.Font.Color:=clActiveCaption;
    redtFormula.Font.Size:=10;
    redtFormula.SetFocus;

    grbResult.Enabled:=true;
    lblResult.Enabled:=true;
    lblResult.Caption:='0';
    lblResult.Hint:='0';

    btnSign.Enabled:=false;

    btnOneDivide.Enabled:=false;
    // ��������� ������� �� �����
    if FileExists(WorkDirectory+'formula.rtf') then
    begin
      redtFormula.Lines.LoadFromFile(WorkDirectory+'formula.rtf');
      MarketText;
    end;
  end;

  Options.StateWork:=3;

end;

procedure TfrmMain.tbtnFullCalcClick(Sender: TObject);
begin
  Height:=335;

  if Options.StateWork=2 then
  begin
    btnBracketOpen.Enabled:=true;
    btnBracketClose.Enabled:=true;

    redtFormula.Clear;
    redtFormula.Font.Color:=clActiveCaption;
    redtFormula.Font.Size:=10;
    redtFormula.SetFocus;

    grbResult.Enabled:=true;
    lblResult.Enabled:=true;
    lblResult.Caption:='0';
    lblResult.Hint:='0';

    btnSign.Enabled:=false;

    btnOneDivide.Enabled:=false;

    // read last expression
    if FileExists(WorkDirectory+'formula.rtf') then
    begin
      redtFormula.Lines.LoadFromFile(WorkDirectory+'formula.rtf');
      MarketText;
    end;

  end;

  Options.StateWork:=1;

end;

procedure TfrmMain.tbtnSimpleCalcClick(Sender: TObject);
begin
  // save expression
  redtFormula.Lines.SaveToFile(WorkDirectory+'formula.rtf');
  
  Options.StateWork:=2;
  Height:=335;

  btnBracketOpen.Enabled:=false;
  btnBracketClose.Enabled:=false;

  redtFormula.Clear;
  redtFormula.Font.Color:=clActiveCaption;
  redtFormula.Font.Size:=12;
  redtFormula.Text:='0';

  grbResult.Enabled:=false;
  if Options.Language then
  begin
    lblResult.Caption:='For Full or Mini';
  end
  else
  begin
    lblResult.Caption:='�� ������������ � ���� ������';
  end;

  btnSign.Enabled:=true;

  btnCalc.SetFocus;

  btnOneDivide.Enabled:=true;
end;

procedure TfrmMain.btnDivideClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('/') then
    begin
      redtFormula.SelText:='/';
      MarketText;
    end;
  end
  else
  begin
    btnCalcClick(Self);
    Operation:=4;
    FirstOper:=StrToFloat(redtFormula.Text);
    Point:=false;
    PressCalc:=false;
    PressOper:=true;
  end;
end;

procedure TfrmMain.btnMultiplieClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('*') then
    begin
      redtFormula.SelText:='*';
      MarketText;
    end;
  end
  else
  begin
    btnCalcClick(Self);
    Operation:=3;
    FirstOper:=StrToFloat(redtFormula.Text);
    Point:=false;
    PressCalc:=false;
    PressOper:=true;
  end;
end;

procedure TfrmMain.btnSubtrackClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('-') then
    begin
      redtFormula.SelText:='-';
      MarketText;
    end;
  end
  else
  begin
    btnCalcClick(Self);
    Operation:=2;
    FirstOper:=StrToFloat(redtFormula.Text);
    Point:=false;
    PressCalc:=false;
    PressOper:=true;
  end;
end;

procedure TfrmMain.btnAddClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('+') then
    begin
      redtFormula.SelText:='+';
      MarketText;
    end;
  end
  else
  begin
    btnCalcClick(Self);
    Operation:=1;
    FirstOper:=StrToFloat(redtFormula.Text);
    Point:=false;
    PressCalc:=false;
    PressOper:=true;
  end;
end;

procedure TfrmMain.redtFormulaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Options.StateWork<>2)and(not(((Key>=33)and(Key<=40))or(Key=16)or(Key=17)or(Key=45)))then MarketText;
end;

procedure TfrmMain.btnBracketOpenClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('(') then
    begin
      redtFormula.SelText:='(';
      MarketText;
    end;
  end
  else
  begin

  end;
end;

procedure TfrmMain.btnBracketCloseClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol(')') then
    begin
      redtFormula.SelText:=')';
      MarketText;
    end;
  end
  else
  begin

  end;
end;

procedure TfrmMain.btnFactClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('!') then
    begin
      redtFormula.SelText:='!';
      MarketText;
    end;
  end
  else
  begin
    if not Point then
      redtFormula.Text:=FloatToStr(Faktorial(StrToInt(redtFormula.Text)));
  end;
end;

procedure TfrmMain.btnDegreeClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('^') then
    begin
      redtFormula.SelText:='^';
      MarketText;
    end;
  end
  else
  begin
    btnCalcClick(Self);
    Operation:=5;
    FirstOper:=StrToFloat(redtFormula.Text);
    Point:=false;
    PressCalc:=false;
    PressOper:=true;
  end;
end;

procedure TfrmMain.btnSinClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('s') then
    begin
      redtFormula.SelText:='sin()';
      redtFormula.SelStart:=redtFormula.SelStart-1;
      MarketText;
    end;
  end
  else
  begin
    if StateGrads=Deg then redtFormula.Text:=FloatToStr(sin(DegToRad(StrToFloat(redtFormula.Text))))
      else if StateGrads=Grad then redtFormula.Text:=FloatToStr(sin(GradToRad(StrToFloat(redtFormula.Text))))
        else redtFormula.Text:=FloatToStr(sin(StrToFloat(redtFormula.Text)));
  end;
end;

procedure TfrmMain.btnCosClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('c') then
    begin
      redtFormula.SelText:='cos()';
      redtFormula.SelStart:=redtFormula.SelStart-1;
      MarketText;
    end;
  end
  else
  begin
    if StateGrads=Deg then redtFormula.Text:=FloatToStr(cos(DegToRad(StrToFloat(redtFormula.Text))))
      else if StateGrads=Grad then redtFormula.Text:=FloatToStr(cos(GradToRad(StrToFloat(redtFormula.Text))))
        else redtFormula.Text:=FloatToStr(cos(StrToFloat(redtFormula.Text)));
  end;
end;

procedure TfrmMain.btnTanClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('t') then
    begin
      redtFormula.SelText:='tan()';
      redtFormula.SelStart:=redtFormula.SelStart-1;
      MarketText;
    end;
  end
  else
  begin
    if StateGrads=Deg then redtFormula.Text:=FloatToStr(tan(DegToRad(StrToFloat(redtFormula.Text))))
      else if StateGrads=Grad then redtFormula.Text:=FloatToStr(tan(GradToRad(StrToFloat(redtFormula.Text))))
        else redtFormula.Text:=FloatToStr(tan(StrToFloat(redtFormula.Text)));
  end;
end;

procedure TfrmMain.btnCtanClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('c') then
    begin
      redtFormula.SelText:='ctan()';
      redtFormula.SelStart:=redtFormula.SelStart-1;
      MarketText;
    end;
  end
  else
  begin
    if StateGrads=Deg then redtFormula.Text:=FloatToStr(cotan(DegToRad(StrToFloat(redtFormula.Text))))
      else if StateGrads=Grad then redtFormula.Text:=FloatToStr(cotan(GradToRad(StrToFloat(redtFormula.Text))))
        else redtFormula.Text:=FloatToStr(cotan(StrToFloat(redtFormula.Text)));
  end;
end;

procedure TfrmMain.btnLnClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('l') then
    begin
      redtFormula.SelText:='ln()';
      redtFormula.SelStart:=redtFormula.SelStart-1;
      MarketText;
    end;
  end
  else
  begin
    redtFormula.Text:=FloatToStr(ln(StrToFloat(redtFormula.Text)));
  end;
end;

procedure TfrmMain.btnLogClick(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('l') then
    begin
      redtFormula.SelText:='log()';
      redtFormula.SelStart:=redtFormula.SelStart-1;
      MarketText;
    end;
  end
  else
  begin
    redtFormula.Text:=FloatToStr(log10(StrToFloat(redtFormula.Text)));
  end;
end;

procedure TfrmMain.btnDegree3Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('^') then
    begin
      redtFormula.SelText:='^3';
      MarketText;
    end;
  end
  else
  begin
    redtFormula.Text:=FloatToStr(Power(StrToFloat(redtFormula.Text),3));
  end;
end;

procedure TfrmMain.btnDegree2Click(Sender: TObject);
begin
  if Options.StateWork<>2 then
  begin
    if CheckSimbol('^') then
    begin
      redtFormula.SelText:='^2';
      MarketText;
    end;
  end
  else
  begin
    redtFormula.Text:=FloatToStr(Power(StrToFloat(redtFormula.Text),2));
  end;
end;

procedure TfrmMain.btnPiClick(Sender: TObject);
var
  Expression:string;
  f,f1,f2,f3,f4:boolean;
begin
  if Options.StateWork<>2 then
  begin
    f:=false;
    f1:=false;
    f2:=false;
    f3:=false;
    f4:=false;
    Expression:=redtFormula.Text;
    if redtFormula.SelStart=0 then f1:=true;
    if redtFormula.SelStart=Length(Expression) then f2:=true;
    if (redtFormula.SelStart>0)and(Expression[redtFormula.SelStart]in['+','-','*','/','^','('])then f3:=true;
    if (redtFormula.SelStart<Length(Expression))and(Expression[redtFormula.SelStart+1]in['+','-','*','/','^',')'])then f4:=true;

    if Length(Expression)<>0 then
    begin
      if f1 and f4 then f:=true;
      if f2 and f3 then f:=true;
      if (not(f2 or f1)) and f3 and f4 then f:=true;
    end
    else f:=true;
    if f then
    begin
      redtFormula.SelText:=FloatToStr(Pi);
      MarketText;
    end;
  end
  else
  begin
    redtFormula.Text:=FloatToStr(Pi);
  end;
end;

procedure TfrmMain.btnExpClick(Sender: TObject);
var
  Expression:string;
  f,f1,f2,f3,f4:boolean;
begin
  if Options.StateWork<>2 then
  begin
    f:=false;
    f1:=false;
    f2:=false;
    f3:=false;
    f4:=false;
    Expression:=redtFormula.Text;
    if redtFormula.SelStart=0 then f1:=true;
    if redtFormula.SelStart=Length(Expression) then f2:=true;
    if (redtFormula.SelStart>0)and(Expression[redtFormula.SelStart]in['+','-','*','/','^','('])then f3:=true;
    if (redtFormula.SelStart<Length(Expression))and(Expression[redtFormula.SelStart+1]in['+','-','*','/','^',')'])then f4:=true;

    if Length(Expression)<>0 then
    begin
      if f1 and f4 then f:=true;
      if f2 and f3 then f:=true;
      if (not(f2 or f1)) and f3 and f4 then f:=true;
    end
    else f:=true;
    if f then
    begin
      redtFormula.SelText:=FloatToStr(2.71828);
      MarketText;
    end;
  end
  else
  begin
    redtFormula.Text:=FloatToStr(2.71828);
  end;
end;

procedure TfrmMain.rbtnRadiansClick(Sender: TObject);
begin
  StateGrads:=Rad;
end;

procedure TfrmMain.rbtnGradsClick(Sender: TObject);
begin
  StateGrads:=Grad;
end;

procedure TfrmMain.rbtnDegreesClick(Sender: TObject);
begin
  StateGrads:=Deg;
end;

procedure TfrmMain.mnuCopyClick(Sender: TObject);
var Clipboard:TClipboard;
begin
  Clipboard:=TClipboard.Create;
  Clipboard.SetTextBuf(PChar(lblResult.Caption));
  Clipboard.Free;
end;

procedure TfrmMain.mnuLessFontClick(Sender: TObject);
begin
  if lblResult.Font.Size>8 then
    lblResult.Font.Size:=lblResult.Font.Size-2;
end;

procedure TfrmMain.mnuBiggerFontClick(Sender: TObject);
begin
  if lblResult.Font.Size<18 then
    lblResult.Font.Size:=lblResult.Font.Size+2;
end;

procedure TfrmMain.redtFormulaChange(Sender: TObject);
begin
  if (Options.StateWork=2)and(redtFormula.Text='') then redtFormula.Text:='0';
  if (Options.StateWork=2)and(not PressCalc)and(Pos(',',redtFormula.Text)=0)then Point:=false;
end;

procedure TfrmMain.btnOneDivideClick(Sender: TObject);
begin
  if StrToFloat(redtFormula.Text)<>0 then
    redtFormula.Text:=FloatToStr(1/StrToFloat(redtFormula.Text));
end;

procedure TfrmMain.tbtnAboutClick(Sender: TObject);
begin
  frmAbout.ShowModal;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  if Options.StateWork=1 then
  begin
    tbtnFullCalcClick(Self);
    tbtnFullCalc.Down:=true;
  end;
  if Options.StateWork=2 then
  begin
    tbtnSimpleCalcClick(Self);
    tbtnSimpleCalc.Down:=true;
  end;
  if Options.StateWork=3 then
  begin
    tbtnMiniCalcClick(Self);
    tbtnMiniCalc.Down:=true;
  end;

  // ��������� ������� �� �����
  if FileExists(WorkDirectory+'formula.rtf') then
  begin
    redtFormula.Lines.LoadFromFile(WorkDirectory+'formula.rtf');
    MarketText;
    if Options.StateWork<>2 then btnCalcClick(Sender);
  end;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var Tray:TNotifyIconData;
begin
  if Options.EnabledTrayIcon then
  begin
    with Tray do
    begin
      cbSize:=SizeOf(TNotifyIconData);
      Wnd:=Handle;
      uId:=1;
      uFlags:=NIF_ICON or NIF_MESSAGE or NIF_TIP;
      uCallBackMessage:=WM_MYTRAYNOTIFY;
      hIcon:=Application.Icon.Handle;
      if Options.Language then
      begin
        StrPCopy(szTip,'Arhimed');
      end
      else
      begin
        StrPCopy(szTip,'�������');
      end
    end;
    Shell_NotifyIcon(NIM_DELETE,@Tray);
    ShowWindow(Application.Handle,SW_SHOWNORMAL);
  end;

  with Options do
  begin
//    Height:=frmMain.Height;
//    Width:=frmMain.Width;
    Left:=frmMain.Left;
    Top:=frmMain.Top;
  end;
  SaveOptions;

  redtFormula.Lines.SaveToFile(WorkDirectory+'formula.rtf');
end;

procedure TfrmMain.tbtnOptionsClick(Sender: TObject);
begin
  frmOptions.ShowModal;
end;

procedure TfrmMain.tbtnEnglishClick(Sender: TObject);
begin
  tbtnEnglish.Down:=True;
  tbtnRussian.Down:=False;

  grbResult.Caption:='Result';

  tbtnFullCalc.Hint:='Full calculator';
  tbtnSimpleCalc.Hint:='Simple calculator';
  tbtnMiniCalc.Hint:='Mini calculator';

  tbtnOptions.Hint:='Options';
  tbtnEnglish.Hint:='English';
  tbtnRussian.Hint:='�������';
  tbtnAbout.Hint:='';

  rbtnGrads.Left:=3;
  rbtnGrads.Caption:='Grads';
  rbtnRadians.Left:=50;
  rbtnRadians.Caption:='Radians';
  rbtnDegrees.Left:=109;
  rbtnDegrees.Caption:='Degrees';

  frmOptions.Caption:='Options';
  frmOptions.chbAlphaBlend.Caption:='Enabled alpha blend';
  frmOptions.lblNumberColor.Caption:='Numbers color';
  frmOptions.chbTrayIcon.Caption:='Enabled tray icon';
  frmOptions.chbStayOnTop.Caption:='Stay on top';
  frmOptions.btnSave.Caption:='Save';
  frmOptions.btnReset.Caption:='Reset';
  frmOptions.btnCancel.Caption:='Cancel';

  frmAbout.Caption:='About';
  frmAbout.lblDescription.Caption:='Simple calculator with 3 mode.';
  frmAbout.redtCopyright.Lines.Text:=  'Copyright (c) 2011-2013, ArusSoft (Alexander Selivanov) - http://www.arussoft.com All rights reserved.'+#13#10+#13#10+
    'Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:'+#13#10+
    '- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.'+#13#10+
    '- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.'+#13#10+
    '- Neither the name of the ArusSoft nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.'+#13#10+#13#10+
    'THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS <AS IS> AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.'+
    ' IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,'+
    ' OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,'+
    ' EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.';

  if Options.StateWork =2 then
  begin
    lblResult.Caption:='For Full or Mini';
  end;

  Options.Language:=true;
  SaveOptions();
end;

procedure TfrmMain.tbtnRussianClick(Sender: TObject);
begin
  tbtnEnglish.Down:=False;
  tbtnRussian.Down:=True;

  grbResult.Caption:='��������� ';

  tbtnFullCalc.Hint:='����������� �����������';
  tbtnSimpleCalc.Hint:='������� �����������';
  tbtnMiniCalc.Hint:='���� �����������';

  tbtnOptions.Hint:='���������';
  tbtnEnglish.Hint:='English';
  tbtnRussian.Hint:='�������';
  tbtnAbout.Hint:='� ���������';

  rbtnGrads.Left:=-3;
  rbtnGrads.Caption:='��������';
  rbtnRadians.Left:=62;
  rbtnRadians.Caption:='������';
  rbtnDegrees.Left:=116;
  rbtnDegrees.Caption:='������';

  frmOptions.Caption:='���������';
  frmOptions.chbAlphaBlend.Caption:='�������� ������������';
  frmOptions.lblNumberColor.Caption:='���� ����';
  frmOptions.chbTrayIcon.Caption:='������ � ����';
  frmOptions.chbStayOnTop.Caption:='������ ����';
  frmOptions.btnSave.Caption:='���������';
  frmOptions.btnReset.Caption:='�����������';
  frmOptions.btnCancel.Caption:='������';

  frmAbout.Caption:='� ���������';
  frmAbout.lblDescription.Caption:='������� ����������� � ����� �������� ������.';
  frmAbout.redtCopyright.Lines.Text:= 'Copyright (c) 2011-2013, ArusSoft (��������� ���������) - http://www.arussoft.ru ��� ����� ��������.'+#13#10+#13#10+
    '����������� ��������� ��������������� � ������������� ��� � ���� ��������� ����, ��� � � �������� �����, � ����������� ��� ���, ��� ���������� ��������� �������:'+#13#10+
    '- ��� ��������� ��������������� ��������� ���� ������ ���������� ��������� ���� ����������� �� ��������� �����, ���� ������ ������� � ����������� ����� �� ��������.'+#13#10+
    '- ��� ��������� ��������������� ��������� ���� ������ ����������� ��������� ���� ���������� �� ��������� �����, ���� ������ ������� � ����������� ����� �� �������� � ������������ �/��� � ������ ����������, ������������ ��� ���������������.'+#13#10+
    '- �� �������� ArusSoft, �� ����� �� ����������� �� ����� ���� ������������ � �������� ��������� ��� ����������� ���������, ���������� �� ���� �� ��� ���������������� ����������� ����������.'+#13#10+#13#10+
    '��� ��������� ������������� ����������� ��������� ���� �/��� ������� ��������� <��� ��� ����> ��� ������-���� ���� ��������, ���������� ���� ��� ���������������, �������, �� �� ������������� ���,'+
    ' ��������������� �������� ������������ �������� � ����������� ��� ���������� ����. �� � ���� ������, ���� �� ��������� ��������������� �������, ��� �� ����������� � ������ �����, �� ���� �������� ��������� ���� � �� ���� ������ ����,'+
    ' ������� ����� �������� �/��� �������� �������������� ���������, ��� ���� ������� ����, �� ��Ѩ� ���������������, ������� ����� �����, ���������, ����������� ��� ������������� ������,'+
    ' ���������� ������������� ��� ������������� ������������� ��������� (�������, �� �� ������������� ������� ������, ��� �������, �������� �������������, ��� �������� ������������ ��-�� ��� ��� ������� ���,'+
    ' ��� ������� ��������� �������� ��������� � ������� �����������), ���� ���� ����� �������� ��� ������ ���� ���� �������� � ����������� ����� �������.';

  if Options.StateWork =2 then
  begin
    lblResult.Caption:='�� ������������';
  end;

  Options.Language:=false;
  SaveOptions();
end;

end.

