unit UOptions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls;

type
  TfrmOptions = class(TForm)
    btnSave: TButton;
    btnCancel: TButton;
    btnReset: TButton;
    chbTrayIcon: TCheckBox;
    chbAlphaBlend: TCheckBox;
    chbStayOnTop: TCheckBox;
    spdAlphaBlend: TSpinEdit;
    clbButtonColor: TColorBox;
    lblNumberColor: TLabel;
    procedure btnSaveClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure chbAlphaBlendClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOptions: TfrmOptions;

implementation

uses UGlobal, UMain, UTypes;

{$R *.dfm}

procedure TfrmOptions.btnSaveClick(Sender: TObject);
begin
  with Options do
  begin
    EnabledTrayIcon:=chbTrayIcon.Checked;

    EnabledAlphaBlend:=chbAlphaBlend.Checked;
    AlphaBlend:=spdAlphaBlend.Value;

    StayOnTop:=chbStayOnTop.Checked;

    ButtonColor:=clbButtonColor.Selected;

    Left:= frmMain.Left;
    Top:=frmMain.Top;
  end;

  frmMain.SetOptions;
  SaveOptions;
  Close;
end;

procedure TfrmOptions.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmOptions.FormShow(Sender: TObject);
begin
  // ��������� ���������
  with Options do
  begin
    chbTrayIcon.Checked:=EnabledTrayIcon;

    chbAlphaBlend.Checked:=EnabledAlphaBlend;
    spdAlphaBlend.Value:=AlphaBlend;

    chbStayOnTop.Checked:=StayOnTop;

    clbButtonColor.Selected:=ButtonColor;
  end;
end;

procedure TfrmOptions.btnResetClick(Sender: TObject);
begin
  // ���������� ����������� ���������
  with Options do
  begin
    chbTrayIcon.Checked:=True;

    chbAlphaBlend.Checked:=False;
    spdAlphaBlend.Value:=0;

    chbStayOnTop.Checked:=false;

    clbButtonColor.Selected:=clBlue;
  end;
end;

procedure TfrmOptions.chbAlphaBlendClick(Sender: TObject);
begin
  spdAlphaBlend.Enabled:=chbAlphaBlend.Checked;
end;

end.
