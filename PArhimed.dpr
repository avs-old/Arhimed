program PArhimed;

uses
  Forms,
  ShellAPI,
  SysUtils,
  Windows,
  IniFiles,
  Classes,
  UMain in 'UMain.pas' {frmMain},
  UAbout in 'UAbout.pas' {frmAbout},
  UOptions in 'UOptions.pas' {frmOptions},
  UTypes in 'UTypes.pas',
  UGlobal in 'UGlobal.pas';

{$R *.res}
begin

  WorkDirectory:=ExtractFilePath(Application.ExeName);
 { if not FileExists(PChar(WorkDirectory+'arhimed.ini')) then
    CreateFile(PChar(WorkDirectory+'arhimed.ini'),GENERIC_READ,FILE_SHARE_READ,nil,CREATE_NEW,FILE_ATTRIBUTE_NORMAL,0)
  else
    SetFileAttributes(PChar(WorkDirectory+'arhimed.ini'),FILE_ATTRIBUTE_ARCHIVE);}
  LoadOptions;

  Application.Initialize;
  Application.Title := '�������';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.CreateForm(TfrmOptions, frmOptions);

  frmMain.SetOptions;

  Application.Run;
end.
