object frmMain: TfrmMain
  Left = 199
  Top = 117
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Arus Arhimed'
  ClientHeight = 301
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010002002020100000000000E80200002600000010101000000000002801
    00000E0300002800000020000000400000000100040000000000800200000000
    0000000000000000000000000000000000000000800000800000008080008000
    0000800080008080000080808000C0C0C0000000FF0000FF000000FFFF00FF00
    0000FF00FF00FFFF0000FFFFFF00000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000007
    7777777777777777777777777770000000000000000000000000000000700000
    0000000000000000000000000070000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000077
    7777777777777777777777777770000000000000000000000000000000700000
    0000000000000000000000000070000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000180000001800000018000
    0003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000018000
    00018000000180000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFF280000001000000020000000010004000000
    0000C00000000000000000000000000000000000000000000000000080000080
    00000080800080000000800080008080000080808000C0C0C0000000FF0000FF
    000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0000000000000000000000
    0000000000000000000000000000000000000000000000777777777777770000
    0000000000070000000000000000000000000000000000000000000000000077
    7777777777770000000000000007000000000000000000000000000000000000
    00000000000000000000000000000000000000000000FFFF0000FFFF0000FFFF
    0000FFFF0000C00000008000000080010000FFFF0000FFFF0000C00000008000
    000080010000FFFF0000FFFF0000FFFF0000FFFF0000}
  KeyPreview = True
  OldCreateOrder = False
  ScreenSnap = True
  SnapBuffer = 15
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 5
    Top = 247
    Width = 18
    Height = 16
    Caption = 'M2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object redtFormula: TRichEdit
    Left = 2
    Top = 2
    Width = 297
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Batang'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = redtFormulaChange
    OnKeyPress = redtFormulaKeyPress
    OnKeyUp = redtFormulaKeyUp
  end
  object grbGrads: TGroupBox
    Left = 128
    Top = 82
    Width = 172
    Height = 25
    TabOrder = 1
    object rbtnGrads: TRadioButton
      Left = 3
      Top = 6
      Width = 65
      Height = 17
      Caption = 'Grads'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = rbtnGradsClick
    end
    object rbtnRadians: TRadioButton
      Left = 50
      Top = 6
      Width = 60
      Height = 17
      Caption = 'Radians'
      TabOrder = 1
      OnClick = rbtnRadiansClick
    end
    object rbtnDegrees: TRadioButton
      Left = 109
      Top = 6
      Width = 60
      Height = 17
      Caption = 'Degrees'
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = rbtnDegreesClick
    end
  end
  object grbResult: TGroupBox
    Left = 115
    Top = 47
    Width = 184
    Height = 35
    Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090' '
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    object lblResult: TLabel
      Left = 4
      Top = 10
      Width = 175
      Height = 22
      Hint = '0'
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clCaptionText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      PopupMenu = mnuResult
      ShowHint = True
      Transparent = True
      WordWrap = True
    end
  end
  object btnNum1: TBitBtn
    Left = 5
    Top = 85
    Width = 37
    Height = 25
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = btnNum1Click
  end
  object btnNum2: TBitBtn
    Left = 45
    Top = 85
    Width = 37
    Height = 25
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = btnNum2Click
  end
  object btnNum3: TBitBtn
    Left = 85
    Top = 85
    Width = 37
    Height = 25
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = btnNum3Click
  end
  object btnNum4: TBitBtn
    Left = 5
    Top = 112
    Width = 37
    Height = 25
    Caption = '4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = btnNum4Click
  end
  object btnNum5: TBitBtn
    Left = 45
    Top = 112
    Width = 37
    Height = 25
    Caption = '5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = btnNum5Click
  end
  object btnNum6: TBitBtn
    Left = 85
    Top = 112
    Width = 37
    Height = 25
    Caption = '6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = btnNum6Click
  end
  object btnNum7: TBitBtn
    Left = 5
    Top = 139
    Width = 37
    Height = 25
    Caption = '7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = btnNum7Click
  end
  object btnNum8: TBitBtn
    Left = 45
    Top = 139
    Width = 37
    Height = 25
    Caption = '8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    OnClick = btnNum8Click
  end
  object btnNum9: TBitBtn
    Left = 85
    Top = 139
    Width = 37
    Height = 25
    Caption = '9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    OnClick = btnNum9Click
  end
  object btnPoint: TBitBtn
    Left = 85
    Top = 166
    Width = 37
    Height = 25
    Caption = ','
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    OnClick = btnPointClick
  end
  object btnNum0: TBitBtn
    Left = 45
    Top = 166
    Width = 37
    Height = 25
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
    OnClick = btnNum0Click
  end
  object btnSign: TBitBtn
    Left = 5
    Top = 166
    Width = 37
    Height = 25
    Caption = '+/-'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 14
  end
  object btnDivide: TBitBtn
    Left = 126
    Top = 136
    Width = 35
    Height = 25
    Caption = '/'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 15
    OnClick = btnDivideClick
  end
  object btnMultiplie: TBitBtn
    Left = 126
    Top = 163
    Width = 35
    Height = 25
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 16
    OnClick = btnMultiplieClick
  end
  object btnSubtrack: TBitBtn
    Left = 126
    Top = 190
    Width = 35
    Height = 25
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 17
    OnClick = btnSubtrackClick
  end
  object btnAdd: TBitBtn
    Left = 126
    Top = 217
    Width = 35
    Height = 25
    Caption = '+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 18
    OnClick = btnAddClick
  end
  object btnMemC: TBitBtn
    Left = 128
    Top = 112
    Width = 30
    Height = 20
    Caption = 'MC'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 19
    OnClick = btnMemCClick
  end
  object btnMemR: TBitBtn
    Left = 159
    Top = 112
    Width = 30
    Height = 20
    Caption = 'MR'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 20
    OnClick = btnMemRClick
  end
  object btnMemSubtrack: TBitBtn
    Left = 190
    Top = 112
    Width = 30
    Height = 20
    Caption = 'M-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 21
    OnClick = btnMemSubtrackClick
  end
  object btnMemAdd: TBitBtn
    Left = 221
    Top = 112
    Width = 30
    Height = 20
    Caption = 'M+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 22
    OnClick = btnMemAddClick
  end
  object grbMemory: TGroupBox
    Left = 254
    Top = 109
    Width = 46
    Height = 23
    TabOrder = 23
    object lblMemory: TLabel
      Left = 4
      Top = 6
      Width = 38
      Height = 16
      Hint = '0'
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
  end
  object btnSin: TBitBtn
    Left = 167
    Top = 136
    Width = 35
    Height = 25
    Caption = 'sin'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clFuchsia
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 24
    OnClick = btnSinClick
  end
  object btnCos: TBitBtn
    Left = 167
    Top = 163
    Width = 35
    Height = 25
    Caption = 'cos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clFuchsia
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 25
    OnClick = btnCosClick
  end
  object btnTan: TBitBtn
    Left = 167
    Top = 190
    Width = 35
    Height = 25
    Caption = 'tan'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clFuchsia
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 26
    OnClick = btnTanClick
  end
  object btnCtan: TBitBtn
    Left = 167
    Top = 217
    Width = 35
    Height = 25
    Caption = 'ctan'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clFuchsia
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 27
    OnClick = btnCtanClick
  end
  object btnBracketClose: TBitBtn
    Left = 255
    Top = 163
    Width = 37
    Height = 25
    Caption = ')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 28
    OnClick = btnBracketCloseClick
  end
  object btnBracketOpen: TBitBtn
    Left = 255
    Top = 136
    Width = 37
    Height = 25
    Caption = '('
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 29
    OnClick = btnBracketOpenClick
  end
  object btnExp: TBitBtn
    Left = 255
    Top = 190
    Width = 37
    Height = 25
    Caption = 'e'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 30
    OnClick = btnExpClick
  end
  object btnLn: TBitBtn
    Left = 207
    Top = 190
    Width = 35
    Height = 25
    Caption = 'ln'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clFuchsia
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 31
    OnClick = btnLnClick
  end
  object btnLog: TBitBtn
    Left = 207
    Top = 217
    Width = 35
    Height = 25
    Caption = 'log'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clFuchsia
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 32
    OnClick = btnLogClick
  end
  object btnFact: TBitBtn
    Left = 207
    Top = 136
    Width = 35
    Height = 25
    Caption = 'n!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 33
    OnClick = btnFactClick
  end
  object btnOneDivide: TBitBtn
    Left = 207
    Top = 163
    Width = 35
    Height = 25
    Caption = '1/x'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 34
    OnClick = btnOneDivideClick
  end
  object btnPi: TBitBtn
    Left = 255
    Top = 217
    Width = 37
    Height = 25
    Caption = 'pi'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 35
    OnClick = btnPiClick
  end
  object btnDegree2: TBitBtn
    Left = 5
    Top = 207
    Width = 37
    Height = 27
    Caption = 'x^2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 36
    OnClick = btnDegree2Click
  end
  object btnDegree3: TBitBtn
    Left = 45
    Top = 207
    Width = 37
    Height = 27
    Caption = 'x^3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 37
    OnClick = btnDegree3Click
  end
  object btnDegree: TBitBtn
    Left = 85
    Top = 207
    Width = 37
    Height = 27
    Caption = 'x^y'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 38
    OnClick = btnDegreeClick
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 272
    Width = 301
    Height = 29
    Align = alBottom
    ButtonHeight = 24
    ButtonWidth = 24
    Caption = 'ToolBar1'
    EdgeBorders = [ebLeft, ebTop]
    Images = imglstMain
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 39
    object tbtnFullCalc: TToolButton
      Left = 3
      Top = 2
      Hint = 'Full calculator'
      Caption = 'tbtnFullCalc'
      Down = True
      Grouped = True
      ImageIndex = 0
      Style = tbsCheck
      OnClick = tbtnFullCalcClick
    end
    object tbtnSimpleCalc: TToolButton
      Left = 27
      Top = 2
      Hint = 'Simple calculator'
      Caption = 'tbtnSimpleCalc'
      Grouped = True
      ImageIndex = 1
      Style = tbsCheck
      OnClick = tbtnSimpleCalcClick
    end
    object tbtnMiniCalc: TToolButton
      Left = 51
      Top = 2
      Hint = 'Mini calculator'
      Caption = 'tbtnMiniCalc'
      Grouped = True
      ImageIndex = 2
      Style = tbsCheck
      OnClick = tbtnMiniCalcClick
    end
    object ToolButton1: TToolButton
      Left = 75
      Top = 2
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 3
      Style = tbsSeparator
    end
    object tbtnOptions: TToolButton
      Left = 83
      Top = 2
      Caption = 'tbtnOptions'
      ImageIndex = 4
      OnClick = tbtnOptionsClick
    end
    object ToolButton2: TToolButton
      Left = 107
      Top = 2
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 115
      Top = 2
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 123
      Top = 2
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 131
      Top = 2
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 139
      Top = 2
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 147
      Top = 2
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 155
      Top = 2
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 163
      Top = 2
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton14: TToolButton
      Left = 171
      Top = 2
      Width = 8
      Caption = 'ToolButton14'
      ImageIndex = 10
      Style = tbsSeparator
    end
    object ToolButton13: TToolButton
      Left = 179
      Top = 2
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 9
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 187
      Top = 2
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton12: TToolButton
      Left = 195
      Top = 2
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object tbtnEnglish: TToolButton
      Left = 203
      Top = 2
      Caption = 'tbtnEnglish'
      ImageIndex = 5
      OnClick = tbtnEnglishClick
    end
    object tbtnRussian: TToolButton
      Left = 227
      Top = 2
      Caption = 'tbtnRussian'
      ImageIndex = 3
      OnClick = tbtnRussianClick
    end
    object ToolButton10: TToolButton
      Left = 251
      Top = 2
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object tbtnAbout: TToolButton
      Left = 259
      Top = 2
      Hint = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
      Caption = 'tbtnAbout'
      ImageIndex = 6
      OnClick = tbtnAboutClick
    end
  end
  object btnClear: TBitBtn
    Left = 2
    Top = 54
    Width = 54
    Height = 25
    Caption = 'C'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 40
    OnClick = btnClearClick
  end
  object btnCalc: TBitBtn
    Left = 56
    Top = 54
    Width = 57
    Height = 25
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clCaptionText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 41
    OnClick = btnCalcClick
  end
  object btnMemC2: TBitBtn
    Left = 31
    Top = 247
    Width = 27
    Height = 20
    Caption = 'MC'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 42
    OnClick = btnMemC2Click
  end
  object btnMemR2: TBitBtn
    Left = 63
    Top = 247
    Width = 27
    Height = 20
    Caption = 'MR'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 43
    OnClick = btnMemR2Click
  end
  object btnMemSubtrack2: TBitBtn
    Left = 95
    Top = 247
    Width = 27
    Height = 20
    Caption = 'M-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 44
    OnClick = btnMemSubtrack2Click
  end
  object btnMemAdd2: TBitBtn
    Left = 127
    Top = 247
    Width = 27
    Height = 20
    Caption = 'M+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 45
    OnClick = btnMemAdd2Click
  end
  object grbMemory2: TGroupBox
    Left = 160
    Top = 247
    Width = 137
    Height = 24
    TabOrder = 46
    object lblMemory2: TLabel
      Left = 3
      Top = 6
      Width = 130
      Height = 16
      Hint = '0'
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
  end
  object imglstMain: TImageList
    Left = 182
    Top = 8
    Bitmap = {
      494C010107000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F4B16B00EF85150000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000008080800080808000000000008080
      80008080800000000000000000000000000000000000D4D4D400C7C7C700C7C7
      C700C7C7C700C7C7C700C7C7C700C7C7C700C7C7C700C7C7C700C7C7C700C7C7
      C700C7C7C700C7C7C700D4D4D400000000000000000000000000000000000000
      00000000000000000000F9DFC300F07E0500F07E0500F3AD6200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000808080008080800080808000808080008080800080808000C0C0
      C00000808000000000000000000000000000000000005F58C0005C418B004901
      01004901010049010100B3B1C4005349AB005046AA00B3B1C400490101004901
      0100490101009B7269009E9BC900000000000000000000000000000000000000
      00000000000000000000EE7D0400EE7D0400F07E0600EE7D0400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      800080808000C0C0C00000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000000000000000000000000000DBDBDB00B5AFC3007D75F1007949
      8C0075191300630E0E00E1DEF5002C19D4002714D300E1DEF50068141500640E
      0E00C6948800C9C6FB00574BB800DBDBDB000000000000000000000000000000
      000000000000F07E0500F07E0500F5BE840000000000EE810D00F07E0500F6C7
      9600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000808080008080800000FFFF0000FFFF00808080000080800000FFFF0000FF
      FF0080808000808080000000000000000000DBDBDB0077353000EADDDF009090
      FF007755B0007F282500E1DEF5002813D400220DD300E1DEF50078212000DABB
      B200B7B9FF00715DD40071353C00DBDBDB000000000000000000000000000000
      0000EE8B2000EE7D0400F09A3D000000000000000000F8DDBF00EE7D0400F07E
      0500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C00000FFFF0000FFFF00C0C0C000C0C0C000008080000080800000FF
      FF0080808000000000000000000000000000DBDBDB005D0F0F00822F2B00E6CF
      C700A2A0FD00765EC800E1DEF5002712D300210DD200E1DEF500E7D6D400A2A1
      FD007257C1008A3C3A00610F0F00DBDBDB00000000000000000000000000F8DE
      C100EE7D0400F17F0600F17F0600F07F0600F07E0500F07E0500EE7D0500F17F
      0600F5BD81000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF000080800000FFFF00C0C0C000C0C0C0000080800000FFFF000080
      800000000000000000000000000000000000DBDBDB00CDCADE00E2DFF500E1DE
      F500E1DEF500E1DEF500E1DEF5002814D400230FD300E1DEF500E1DEF500E1DE
      F500E1DEF500E2DFF500CDCADE00DBDBDB00000000000000000000000000ED81
      0C00EE7D0400F17F0600F17F0600F17F0600F17F0600F17F0600F17F0600F17F
      0600F07E05000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF00C0C0C000C0C0C000C0C0C000C0C0C0000080800080808000C0C0
      C00000000000000000000000000000000000DBDBDB003D2DC9003320D5002511
      D2002511D3002510D200230FD2002713D3002713D300230FD2002410D2002511
      D3002511D2003320D5003C2CC900DBDBDB00000000000000000000000000F8D8
      B500F9D8B500F9D8B500F9D8B500F9D8B500F9D8B500F9D8B500F9D8B500F9D8
      B500F9D8B5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000808080000000
      000000000000000000000000000000000000DBDBDB004636D2005544DC004635
      D9004634D9004634D9004533D9004736D9004635D9004634D9004736D9004634
      D9004634D9005545DD004738D300DBDBDB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000808080000000
      000000000000000000000000000000000000DBDBDB00E3E1F400EAE8F900E8E5
      F800E8E5F800E8E5F800E8E5F8005749DD005545DD00E8E5F800E8E5F800E8E5
      F800E8E5F800EAE8F900E3E1F400DBDBDB000000000000000000EE800B00F07E
      0500F07E0500EF7E0500F07E0600EF7D0400EC810C00EE841300EE841400EF85
      1500EF881A00EE810D0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000C0C0C00080808000C0C0C000C0C0C000C0C0C000C0C0C000808080000000
      000000000000000000000000000000000000DBDBDB0086383800B17976009E88
      C800B2AFFB00EDE4E700EAE7F8006657E0006253E000EAE7F800A490D000B2B0
      FB00F0E5E400B3817E0082383800DBDBDB0000000000F9E2CB00F17E0500F17F
      0600F17F0600EE7D0500F17F0500F17F0600F17F0500F07E0600F17F0600F17F
      0600F17F0600F07E0500F7D0A900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      80008080800080808000C0C0C000C0C0C000C0C0C000C0C0C000808080000000
      000000000000000000000000000000000000DEDEDE00A0616200AC9DDD00DFDD
      FC00E7D3CD00B4838100EDECFA008378E6008175E600EDECFA00B7858100B39C
      C900BAB8FE00F3EEF300AF777100DEDEDE0000000000ED800A00F17F0600ED82
      0E00FBEBD900FBEBD900FBEBD900FBEBD900FBEBD900FBEBD900FBEBD900FBEB
      D900F2AA5E00F17F0600F07E0500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C0000000
      000000000000000000000000000000000000000000008D7FDF00C8C9FF00D1AB
      A200914A490088414100E8E5F8005A4ADD005647DD00E8E5F80090484900954A
      44009571A500948DF200EAE6F6000000000000000000F07F0500F17F0500F5DC
      C200E2B88B00DEA97200CE7F2B00DA99530000000000D9995400E2AE7700F3E2
      D200E3B58200F07E0600F17F0600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EE7D0400EE7D04000000
      0000D075140000000000D3832F00D5934C00F5E3D200E2AD7300EABF9000EFD7
      BC00EDD7C000EF7F0400EE7F0500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FAEADA000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F7D7B400F9DFC300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFF0000FFFFFFFFFE7F0000
      FD278001FC3F0000F8078001FC3F0000E00F0000F88F0000E0030000F18F0000
      E0070000E0070000E0070000E0070000E00F0000E0070000E01F0000FFFF0000
      E01F0000C0030000E01F000080010000E01F000080010000E01F800180810000
      FFFFFFFF94010000FFFFFFFFDFF90000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FC3FFC3FFC3FFFFFF3CFF3CFF3CF0000EC77EC37EE770000DEF7DDF7DDB70000
      DEFBDDFBDFBB0000DEFBDEFBDFBB0000DEFBDF7BDE7B0000DEFBDFBBDFBB0000
      DCF7DDB7DDB70000EEF7EE77EE770000E7EFE7EFE7EF0000F81FF81FF81FFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object mnuResult: TPopupMenu
    Left = 273
    Top = 50
    object mnuCopy: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100
      OnClick = mnuCopyClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object mnuLessFont: TMenuItem
      Caption = #1059#1084#1077#1085#1100#1096#1080#1090#1100' '#1096#1088#1080#1092#1090
      OnClick = mnuLessFontClick
    end
    object mnuBiggerFont: TMenuItem
      Caption = #1059#1074#1077#1083#1080#1095#1080#1090#1100' '#1096#1088#1080#1092#1090
      OnClick = mnuBiggerFontClick
    end
  end
  object XPMan: TXPManifest
    Left = 264
    Top = 8
  end
end
